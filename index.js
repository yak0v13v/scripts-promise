"use strict";

function getTickets() {

    return new Promise((resolve, reject) => {
        console.log('Получение tickets');
    setTimeout(function () {
        let success = true; // успешное выполнение запроса
        if (success) {
            resolve({ id: 1, name: 'петров Михаил', flyight: 'AR12322', seat: '22f' })
        } else {
            reject('Ошибка получения tickets');
        }
    }, 1000);
})
}

function getOrders(tickets) {
    return new Promise((resolve, reject) => {
        console.log('Получение hotel')
    setTimeout(function () {
        let success = true; // успешное выполнение запроса
        if (success) {
            resolve([
                { id: 10, customer_id: 1, room_id: 1 },
                { id: 11, customer_id: 1, room_id: 2 }
            ])
        } else {
            reject('Ошибка получения hotel');
        }
    }, 1000);
})
}

function getVisa(order) {
    return new Promise((resolve, reject) => {
        console.log('Получение visa');
    setTimeout(function () {
        let success = true; // успешное выполнение запроса
        if (success) {
            resolve({ id: 123, name: 'Петров Михаил', country: 'France' },)
        } else {
            reject('Ошибка получения visa');
        }
    }, 1000);
})
}

let resultOrder = [];
let promiseGetTickets= getTickets()
    .then(tickets => getOrders(tickets))
.then(orders => {
    Promise.all(
    orders.map(order => getVisa(order))
).then(_visa => console.log(_visa));
})
.catch(err => console.error(err));